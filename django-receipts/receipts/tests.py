from django.test import TestCase
from decimal import Decimal
from receipts.models import Receipt
# Create your tests here.


class ReceiptTest(TestCase):
    fixtures = ['receipts.json', ]

    def test_receipt(self):
        receipt = Receipt.objects.get(id=1)
        total = receipt.total()

        expected = Decimal('37.55')
        self.assertEqual(expected, total)


