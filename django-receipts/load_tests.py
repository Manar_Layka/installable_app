import sys
from unittest import TestSuite
from boot_django import boot_django

boot_django()

default_labels = ['receipts.tests', ]


def get_suite(labels=default_labels):
    from django.test.runner import DiscoverRunner     # setting up the test environment and its data
    runner = DiscoverRunner(verbosity=1)
    failures = runner.run_tests(labels)
    if failures:
        sys.exit(failures)

    return TestSuite()


# here i can pass arguments to get_suite(). ex: test file i wanna apply it on this app
if __name__ == '__main__':
    labels = default_labels
    if len(sys.argv[1:]) > 0:
        labels = sys.argv[1:]

    get_suite(labels)
